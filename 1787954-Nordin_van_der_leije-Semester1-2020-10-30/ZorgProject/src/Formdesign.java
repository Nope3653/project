import javax.swing.*;

final class Formdesign
{ 
    protected final JFrame startFrame = new JFrame();
    protected final JFrame userFrame = new JFrame();
    protected final JFrame medicineFrame = new JFrame();

    //makes a button.
    protected JButton makeButton(JFrame jFrame, int xAxis, int yAxis, int length, int width, String text)
    {
        JButton jButton = new JButton(text);
        jButton.setBounds(xAxis,yAxis,length,width);
        jFrame.add(jButton);

        return jButton;
    }

    //sets the layout of the frame
    protected void createFrameLayout(JFrame jFrame, int width, int length, boolean setVisible)
    {
        jFrame.setSize(width, length);
        jFrame.setLayout(null);
        jFrame.setVisible(setVisible);
    }

    //makes a label on the frame.
    protected void makeLabel(JFrame jFrame, String text, int xAxis, int yAxis, int length, int width)
    {
        JLabel jLabel = new JLabel(text);
        jLabel.setBounds(xAxis,yAxis,length,width);
        jFrame.add(jLabel);
    }

    //makes a text area on the frame.
    protected  JTextField makeTextField(JFrame jFrame, String text, int xAxis, int yAxis, int length, int width)
    {
        JTextField jTextField = new JTextField(text);
        jTextField.setBounds(xAxis,yAxis,length,width);
        jFrame.add(jTextField);

        return jTextField;
    }
}