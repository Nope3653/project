import static org.junit.Assert.assertEquals;

final class UnitTest 
{
    protected void test(Object expected, Object actual)
    {
        assertEquals(expected, actual);
    }
}