final class Profile
{ 
    private String firstname;
    private String lastname;
    private int age;
    private double weight;
    private double length;

    protected Profile(String firstname, String lastname, int age, double weight, double length) 
    {
        this.firstname = firstname;
        this.lastname = lastname;
        this.age = age;
        this.weight = weight;
        this.length = length;
    }

    protected String getFirstname() 
    {
        return firstname;
    }

    protected void setFirstname(String newfirstname) 
    {
        this.firstname = newfirstname;
    }

    protected String getLastname()
    {
        return lastname;
    }

    protected void setLastname(String newlastname)
    {
        this.lastname = newlastname;
    }

    protected int getAge()
    {
        return age;
    }

    protected void setAge(int newAge)
    {
        this.age = newAge;
    }

    protected double getWeight()
    {
        return weight;
    }

    protected void setWeight(double newWeight)
    {
        this.weight = newWeight;
    }

    protected double getLenght()
    {
        return length;
    }

    protected void setLength(double newlength)
    {
        this.length = newlength;
    }

    protected String bmi()
    {
        return Double.toString(Math.round(weight / Math.pow(length, 2) * 100.0) / 100.0);
    }
}