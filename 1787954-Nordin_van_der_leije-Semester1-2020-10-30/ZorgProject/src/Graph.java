import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

final class Graph extends JPanel
{
    private final Json json = new Json();
    private static final long serialVersionUID = 2405172041950251807L;
    private int padding = 25;
    private int labelPadding = 35;
    private Color lineColor = new Color(44, 102, 230, 180);
    private Color pointColor = new Color(100, 100, 100, 180);
    private Color gridColor = new Color(200, 200, 200, 200);
    private int pointWidth = 6;

    @Override
    protected void paintComponent(Graphics g) 
    {
        Graphics2D g2 = (Graphics2D) g;

        double xScale = ((double) getWidth() - (2 * padding) - labelPadding) / (json.getWeightDays().size() - 1);
        double yScale = ((double) getHeight() - 2 * padding - labelPadding) / (getMaxScore() - getMinScore());

        List<Point> graphPoints = new ArrayList<>();
        
        for (int i = 0; i < json.getWeightDays().size(); i++) 
        {
            int x1 = (int) (i * xScale + padding + labelPadding);
            int y1 = (int) ((getMaxScore() - json.getWeights()[i]) * yScale + padding);
            graphPoints.add(new Point(x1, y1));
        }
        
        g2.setColor(Color.WHITE);
        g2.setColor(Color.BLACK);

        int numberYDivisions = (int) (getMaxScore() - getMinScore());
        
        //yAxis
        for (int i = 0; i < numberYDivisions + 1; i++) 
        {
            int x0 = padding + labelPadding;
            int x1 = pointWidth + padding + labelPadding;
            int y0 = getHeight() - ((i * (getHeight() - padding * 2 - labelPadding)) / numberYDivisions + padding + labelPadding);
            int y1 = y0;

            if (!json.getWeightDays().isEmpty())
            {
                g2.setColor(gridColor);
                g2.drawLine(padding + labelPadding + 1 + pointWidth, y0, getWidth() - padding, y1);
                g2.setColor(Color.BLACK);
                String yLabel = ((int) ((getMinScore() + (getMaxScore() - getMinScore()) * ((i * 1.0) / numberYDivisions)) * 100)) / 100.0 + "";
                g2.drawString(yLabel, x0 - 22 - 5, y0 + (16 / 2) - 3);
                g2.drawString("KG",8, (getHeight()) / 2); 
            }

            g2.drawLine(x0, y0, x1, y1);
        }

        String days = "";
        String time = "";

        //xAxis
        for (int i = 0; i < json.getWeightDays().size(); i++) 
        {
            days = json.getWeightDays().get(i).split(" ")[0];
            time = json.getWeightDays().get(i).split(" ")[1];

            if (json.getWeightDays().size() > 1) 
            {
                int x0 = i * (getWidth() - padding * 2 - labelPadding) / (json.getWeightDays().size() - 1) + padding + labelPadding;
                int x1 = x0;
                int y0 = getHeight() - padding - labelPadding;
                int y1 = y0 - pointWidth;

                if ((i % ((int) (json.getWeightDays().size() / 20.0) + 1)) == 0) 
                {
                    g2.setColor(gridColor);
                    g2.drawLine(x0, getHeight() - padding - labelPadding - 1 - pointWidth, x1, padding);
                    g2.setColor(Color.BLACK);
                    g2.drawString(days, x0 - 70 / 2, y0 + 16 + 3);
                    g2.drawString(time, x0 - 50 / 2, y0 + 16 + 15);
                    g2.drawString(json.getLanguage(ZorgApp.getLanguage())[18],getWidth() / 2,557); 
                }  

                g2.drawLine(x0, y0, x1, y1);
            }
        }

        g2.setColor(lineColor);

        //draw line
        for (int i = 0; i < graphPoints.size() - 1; i++) 
        {
            int x1 = graphPoints.get(i).x;
            int y1 = graphPoints.get(i).y;
            int x2 = graphPoints.get(i + 1).x;
            int y2 = graphPoints.get(i + 1).y;
            g2.drawLine(x1, y1, x2, y2);
        }

        //data points on line
        g2.setColor(pointColor);

        for (int i = 0; i < graphPoints.size(); i++) 
        {
            int x = graphPoints.get(i).x - pointWidth / 2;
            int y = graphPoints.get(i).y - pointWidth / 2;
            int ovalW = pointWidth;
            int ovalH = pointWidth;
            g2.fillOval(x, y, ovalW, ovalH);
        }
    }

    private double getMinScore() 
    {
        double minScore = Double.MAX_VALUE;

        for (Double score : json.getWeights()) 
        {
            minScore = Math.min(minScore, score);
        }

        return Math.round(minScore);
    }

    private double getMaxScore() 
    {
        double maxScore = Double.MIN_VALUE;

        for (Double score : json.getWeights()) 
        {
            maxScore = Math.max(maxScore, score);
        }

        return Math.ceil(maxScore);
    }
    
    protected void createAndShowGui() 
    {
        Graph mainPanel = new Graph();

        JFrame frame = new JFrame("DrawGraph");

        double begin = json.getWeights()[0];
        double end = json.getWeights()[json.getWeights().length - 1];

        if(begin > end)
        {
            JLabel label = new JLabel(json.getLanguage(ZorgApp.getLanguage())[7]);
            label.setBounds(10,545,200,15);
            frame.add(label);
        }
        else if(end > begin)
        {
            JLabel label = new JLabel(json.getLanguage(ZorgApp.getLanguage())[8]);
            label.setBounds(10,545,220,15);
            frame.add(label);
        }

        frame.getContentPane().add(mainPanel);
        frame.setSize(800, 600);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
}