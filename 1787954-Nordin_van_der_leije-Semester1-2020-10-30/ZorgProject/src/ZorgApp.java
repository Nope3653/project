import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import javax.swing.*;
import java.awt.event.*;

final class ZorgApp 
{
    private final ArrayList<Medicine> medicineList = new ArrayList<>();
    private final Formdesign desgin = new Formdesign();
    private final Json json = new Json();
    private final Graph weightgraph = new Graph();
    private static final String[] languageString = { "NL", "EN", "FR"}; 
    private Profile profile = new Profile(json.getUserDetails("firstName").get(0).toString(), json.getUserDetails("lastName").get(0).toString(), Integer.parseInt(json.getUserDetails("age").get(0).toString()), json.getWeights()[json.getWeights().length - 1], Double.parseDouble(json.getUserDetails("length").get(0).toString()));
    private static JComboBox<String> jComboBox = new JComboBox<>(languageString);
    private UnitTest unitTest = new UnitTest();

    protected ZorgApp()
    {   
        jComboBox.setBounds(330, 10, 50, 20);
        desgin.startFrame.add(jComboBox); 

        JButton button = desgin.makeButton(desgin.startFrame, 100, 100, 180, 50, json.getLanguage(getLanguage())[3]);
        JButton jButton = desgin.makeButton(desgin.startFrame, 100, 180, 180, 50, json.getLanguage(getLanguage())[4]);    
                      
        jComboBox.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e) 
            {
                new SwingWorker<Void, Void>() 
                {
                    @Override
                    protected Void doInBackground() throws Exception 
                    {
                        button.setText(json.getLanguage(getLanguage())[3]);
                        jButton.setText(json.getLanguage(getLanguage())[4]);
                        return null;
                    }
                }.execute();    
            }
        });

       button.addActionListener(e -> makePatient());
       jButton.addActionListener(e -> makePatient());

        desgin.createFrameLayout(desgin.startFrame, 400, 400, true);
    }

    protected static String getLanguage() 
    { 
        return jComboBox.getSelectedItem().toString();
    } 

    private void editMedicine()
    {
        JTextField medicineNamefield1 = desgin.makeTextField(desgin.medicineFrame, medicineList.get(0).getMedicineName(), 10, 70, 80, 20);
        JTextField medicineNamefield2 = desgin.makeTextField(desgin.medicineFrame, medicineList.get(1).getMedicineName(), 10, 100, 80, 20);
        JTextField medicineNamefield3 = desgin.makeTextField(desgin.medicineFrame, medicineList.get(2).getMedicineName(), 10, 130, 80, 20);
        JTextField descriptionfield1 = desgin.makeTextField(desgin.medicineFrame, medicineList.get(0).getDescription(), 150, 70, 220, 20);
        JTextField descriptionfield2 = desgin.makeTextField(desgin.medicineFrame, medicineList.get(1).getDescription(), 150, 100, 220, 20);
        JTextField descriptionfield3 = desgin.makeTextField(desgin.medicineFrame, medicineList.get(2).getDescription(), 150, 130, 220, 20);
        JTextField dosagefield1 = desgin.makeTextField(desgin.medicineFrame, medicineList.get(0).getDosage(), 450, 70, 80, 20);
        JTextField dosagefield2 = desgin.makeTextField(desgin.medicineFrame, medicineList.get(1).getDosage(), 450, 100, 80, 20);
        JTextField dosagefield3 = desgin.makeTextField(desgin.medicineFrame, medicineList.get(2).getDosage(), 450, 130, 80, 20);
        JTextField sortfield1 = desgin.makeTextField(desgin.medicineFrame, medicineList.get(0).getSort(), 600, 70, 30, 20);
        JTextField sortfield2 = desgin.makeTextField(desgin.medicineFrame, medicineList.get(1).getSort(), 600, 100, 30, 20);
        JTextField sortfield3 = desgin.makeTextField(desgin.medicineFrame, medicineList.get(2).getSort(), 600, 130, 30, 20);

        JButton button = desgin.makeButton(desgin.medicineFrame, 10, 180, 100, 20, json.getLanguage(getLanguage())[5]);

        button.addActionListener(e -> 
        {
            String[] medicineNames = {medicineNamefield1.getText(), medicineNamefield2.getText(), medicineNamefield3.getText()};
            String[] discriptions = {descriptionfield1.getText(), descriptionfield2.getText(), descriptionfield3.getText()};
            String[] sort = {sortfield1.getText(), sortfield2.getText(), sortfield3.getText()};
            String[] dosage = {dosagefield1.getText(), dosagefield2.getText(), dosagefield3.getText()};

            json.setUserDetail(discriptions, "description");
            json.setUserDetail(medicineNames, "medicineName");
            json.setUserDetail(dosage, "dosage");
            json.setUserDetail(sort, "sort");
        });
    }

    //make's the medicine for the patient.
    private void makeMedicine()
    {
        String medicineName = "medicineName";
        String description = "description";
        String sort = "sort";
        String dosage = "dosage";

        creatMedicineList(json.getUserDetails(medicineName).get(0).toString(), json.getUserDetails(description).get(0).toString(), json.getUserDetails(dosage).get(0).toString(), json.getUserDetails(sort).get(0).toString());
        creatMedicineList(json.getUserDetails(medicineName).get(1).toString(), json.getUserDetails(description).get(1).toString(), json.getUserDetails(dosage).get(1).toString(), json.getUserDetails(sort).get(1).toString());      
        creatMedicineList(json.getUserDetails(medicineName).get(2).toString(), json.getUserDetails(description).get(2).toString(), json.getUserDetails(dosage).get(2).toString(), json.getUserDetails(sort).get(2).toString());

        editMedicine();

        desgin.userFrame.setVisible(false);

        desgin.makeLabel(desgin.medicineFrame, json.getLanguage(getLanguage())[16], 10, 40, 150, 20);
        desgin.makeLabel(desgin.medicineFrame, json.getLanguage(getLanguage())[0], 150, 40, 150, 20);
        desgin.makeLabel(desgin.medicineFrame, json.getLanguage(getLanguage())[2], 450, 40, 100, 20);
        desgin.makeLabel(desgin.medicineFrame, json.getLanguage(getLanguage())[1], 600, 40, 100, 20);

        desgin.createFrameLayout(desgin.medicineFrame, 700, 250, true);
        
        unitTest.test(json.getUserDetails(medicineName).get(0).toString() ,medicineList.get(0).getMedicineName());
        unitTest.test(json.getUserDetails(medicineName).get(1).toString() ,medicineList.get(1).getMedicineName());
        unitTest.test(json.getUserDetails(medicineName).get(2).toString() ,medicineList.get(2).getMedicineName());
        unitTest.test(json.getUserDetails(description).get(0).toString() ,medicineList.get(0).getDescription());
        unitTest.test(json.getUserDetails(description).get(1).toString() ,medicineList.get(1).getDescription());
        unitTest.test(json.getUserDetails(description).get(2).toString() ,medicineList.get(2).getDescription());
        unitTest.test(json.getUserDetails(dosage).get(0).toString() ,medicineList.get(0).getDosage());
        unitTest.test(json.getUserDetails(dosage).get(1).toString() ,medicineList.get(1).getDosage());
        unitTest.test(json.getUserDetails(dosage).get(2).toString() ,medicineList.get(2).getDosage());
        unitTest.test(json.getUserDetails(sort).get(0).toString() ,medicineList.get(0).getSort());
        unitTest.test(json.getUserDetails(sort).get(1).toString() ,medicineList.get(1).getSort());
        unitTest.test(json.getUserDetails(sort).get(2).toString() ,medicineList.get(2).getSort());
    }

    // editing function.
    private void editUser() 
    {
        JTextField weightField = desgin.makeTextField(desgin.userFrame, Double.toString(profile.getWeight()), 100, 130,50, 20);
        JTextField lengthField = desgin.makeTextField(desgin.userFrame, Double.toString(profile.getLenght()), 100, 160,50, 20);
        JTextField firstnameField = desgin.makeTextField(desgin.userFrame, profile.getFirstname(), 100, 40, 50, 20);
        JTextField lastnameField = desgin.makeTextField(desgin.userFrame, profile.getLastname(), 100, 70, 50, 20);
        JTextField ageField = desgin.makeTextField(desgin.userFrame, Integer.toString(profile.getAge()), 100, 100, 50,20);

        JButton button = desgin.makeButton(desgin.userFrame, 10, 230, 100, 20, json.getLanguage(getLanguage())[5]);
        JButton addWeightButton = desgin.makeButton(desgin.userFrame, 200, 130, 185, 20, json.getLanguage(getLanguage())[20]);

        addWeightButton.addActionListener(e -> 
        {
            String weight = weightField.getText();

            LocalDateTime localDate = LocalDateTime.now();
            DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            String formattedDate = localDate.format(myFormatObj);

            json.setWeight(formattedDate, weight);
        });

        button.addActionListener(e -> 
        {
            double weight = Double.parseDouble(weightField.getText());
            double length = Double.parseDouble(lengthField.getText());
            String firstname = firstnameField.getText();
            String lastname = lastnameField.getText();
            int age = Integer.parseInt(ageField.getText());

            profile.setWeight(weight);
            profile.setLength(length);
            profile.setFirstname(firstname);
            profile.setLastname(lastname);
            profile.setAge(age); 
            
            json.editWeight(Double.toString(weight));
            json.setUserDetail(new String[] {firstname}, "firstName");
            json.setUserDetail(new String[] {lastname}, "lastName");
            json.setUserDetail(new String[] {Integer.toString(age)}, "age");
            json.setUserDetail(new String[] {Double.toString(length)}, "length");
            
            desgin.makeTextField(desgin.userFrame, profile.bmi(), 100, 190, 50, 20);
        });
    }

    // make's a patient.
    private void makePatient() 
    {
        editUser();

        JButton profileButton = desgin.makeButton(desgin.medicineFrame,10, 10, 80, 20, json.getLanguage(getLanguage())[15]);
        JButton medicine = desgin.makeButton(desgin.userFrame,120, 10, 110, 20,json.getLanguage(getLanguage())[16]);
        JButton graph = desgin.makeButton(desgin.userFrame, 10, 280, 100, 20, json.getLanguage(getLanguage())[17]);

        graph.addActionListener(e -> weightgraph.createAndShowGui());
        
        profileButton.addActionListener(e -> 
        {
            desgin.userFrame.setVisible(true);
            desgin.medicineFrame.setVisible(false);
        });

        medicine.addActionListener(e -> makeMedicine());

        desgin.startFrame.setVisible(false);
        desgin.createFrameLayout(desgin.userFrame, 400, 400, true);
           
        desgin.makeTextField(desgin.userFrame,profile.bmi(), 100, 190, 50, 20);

        desgin.makeLabel(desgin.userFrame,json.getLanguage(getLanguage())[9], 10, 40, 70, 20);
        desgin.makeLabel(desgin.userFrame,json.getLanguage(getLanguage())[10], 10, 70, 90, 20);
        desgin.makeLabel(desgin.userFrame,json.getLanguage(getLanguage())[11], 10, 100, 60, 20);
        desgin.makeLabel(desgin.userFrame,json.getLanguage(getLanguage())[14], 160, 100, 60, 20);
        desgin.makeLabel(desgin.userFrame,json.getLanguage(getLanguage())[12], 10, 130, 60, 20);
        desgin.makeLabel(desgin.userFrame,"KG", 160, 130, 60, 20);
        desgin.makeLabel(desgin.userFrame,json.getLanguage(getLanguage())[13], 10, 160, 60, 20);
        desgin.makeLabel(desgin.userFrame,"M", 160, 160, 60, 20);
        desgin.makeLabel(desgin.userFrame,json.getLanguage(getLanguage())[6], 10, 190, 60, 20);  

        unitTest.test(json.getUserDetails("firstName").get(0).toString(),profile.getFirstname());
        unitTest.test(json.getUserDetails("lastName").get(0).toString(),profile.getLastname());
        unitTest.test(Integer.parseInt(json.getUserDetails("age").get(0).toString()),profile.getAge());
        unitTest.test(Double.toString(Math.round(profile.getWeight() / Math.pow(profile.getLenght(), 2) * 100.0) / 100.0),profile.bmi());
    }

    //makes a array list
    private ArrayList<Medicine> creatMedicineList(String medicineName, String description, String dosage, String sort)
    {
        Medicine medicine = new Medicine();
        medicine.setMedicineName(medicineName);
        medicine.setDescription(description);
        medicine.setDosage(dosage);
        medicine.setSort(sort);
        
        medicineList.add(medicine);

        return medicineList;
    }
}