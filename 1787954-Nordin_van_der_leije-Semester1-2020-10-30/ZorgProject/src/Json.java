import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.io.File;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

final class Json
{
    private final JSONParser jsonParser = new JSONParser();
    private final String filePath = "PatientInfo.json";
    private final String filePath1 = "Language.json";
    private final String key = "User1";
    private final String weight = "weight";

    protected String[] getLanguage(String language)
    {
        try
        {
            JSONArray jsonArray = (JSONArray) jsonParser.parse(new FileReader(filePath1));

            int index;

            switch(language)
            {
                case "NL" -> index = 0;
                case "EN" -> index = 1;
                case "FR" -> index = 2;
                default -> index = 0;
            }

            JSONObject jsonObject = (JSONObject) jsonArray.get(index);
            JSONObject userObject = (JSONObject) jsonObject.get(language); 
            
            Set<?> set =  userObject.keySet();

            Iterator<?> iterator = set.iterator();

            String[] languages = new String[set.size()];

            for(int increment = 0; increment < set.size(); increment++)
            {
                languages[increment] = (String) userObject.get(iterator.next());
            }
            
            return languages;
            
        } 
        catch (Exception exception) 
        {
            return createAndReadLanguageFile(language);
        }
    }

    @SuppressWarnings("unchecked")
   protected void editWeight(String value)
   {
        try
        {
            JSONArray jsonArray = (JSONArray) jsonParser.parse(new FileReader(filePath));

            JSONObject jsonObject = (JSONObject) jsonArray.get(0);
            JSONObject jObject = (JSONObject) jsonObject.get(key);
            JSONObject user = (JSONObject) jObject.get(weight);

            String lastDay = getWeightDays().get(getWeightDays().size() - 1);

            user.put(lastDay, value);
            
            JSONObject userObject = new JSONObject();
            userObject.putAll(jsonObject);

            JSONArray userList = new JSONArray();
            userList.add(userObject);

            try (FileWriter file = new FileWriter(filePath)) 
            {
                file.write(userList.toJSONString());
                file.flush();
            }
        } 
        catch (Exception exception) 
        {
            exception.printStackTrace();
        }
   }

    @SuppressWarnings("unchecked")
    protected void setWeight(String date, String value)
    {
        try
        {
            JSONArray jsonArray = (JSONArray) jsonParser.parse(new FileReader(filePath));

            JSONObject jsonObject = (JSONObject) jsonArray.get(0);
            JSONObject jObject = (JSONObject) jsonObject.get(key);
            JSONObject user = (JSONObject) jObject.get(weight);
            user.put(date, value);

            JSONObject userObject = new JSONObject();
            userObject.putAll(jsonObject);

            JSONArray userList = new JSONArray();
            userList.add(userObject);

            try (FileWriter file = new FileWriter(filePath)) 
            {
                file.write(userList.toJSONString());
                file.flush();
            }
        } 
        catch (Exception exception) 
        {
            exception.printStackTrace();
        }
    }

    protected Double[] getWeights()
    {
        try
        {
            JSONArray jsonArray = (JSONArray) jsonParser.parse(new FileReader(filePath));

            JSONObject jsonObject = (JSONObject) jsonArray.get(0);
            JSONObject jObject = (JSONObject) jsonObject.get(key);
            JSONObject user = (JSONObject) jObject.get(weight);

            Double[] weights = new Double[getWeightDays().size()];

            for(int increment = 0; increment < weights.length; increment++)
            {
                weights[increment] = Double.parseDouble(user.get(getWeightDays().get(increment)).toString());
            }

            return weights;
        } 
        catch (Exception exception) 
        {
            exception.printStackTrace();
        }

        return new Double[] {};
    }

    protected List<String> getWeightDays()
    {
        List<String> date = new ArrayList<>();

        try
        {
            JSONArray jsonArray = (JSONArray) jsonParser.parse(new FileReader(filePath));

            JSONObject jsonObject = (JSONObject) jsonArray.get(0);
            JSONObject jObject = (JSONObject) jsonObject.get(key);
            JSONObject user = (JSONObject) jObject.get(weight);

            Set<?> set =  user.keySet();

            Iterator<?> iterator = set.iterator();

            while(iterator.hasNext())
            {
                date.add(iterator.next().toString());
            }

            Collections.sort(date);  

            return date;
        } 
        catch (Exception exception) 
        {
            exception.printStackTrace();
        }

        return date;
    }

    protected JSONArray getUserDetails(String value) 
    {
        try
        {
            JSONArray jsonArray = (JSONArray) jsonParser.parse(new FileReader(filePath));

            JSONObject jsonObject = (JSONObject) jsonArray.get(0);  
            JSONObject jObject = (JSONObject) jsonObject.get(key);      
            return (JSONArray) jObject.get(value);
        } 
        catch (Exception exception) 
        {
            return createAndReadUserFile(value);
        }
    }

    @SuppressWarnings("unchecked")
    protected void setUserDetail(String[] change, String getKey) 
    {
        try
        {
            JSONArray jsonArray = (JSONArray) jsonParser.parse(new FileReader(filePath));

            JSONObject jsonObject = (JSONObject) jsonArray.get(0);
            JSONObject jObject = (JSONObject) jsonObject.get(key);

            JSONArray jArray = new JSONArray();

            for(int increment = 0; increment < change.length; increment++)
            {
                jArray.add(change[increment]);
            }

            jObject.put(getKey, jArray);

            JSONObject userObject = new JSONObject();
            userObject.put(key, jObject);

            JSONArray userList = new JSONArray();
            userList.add(userObject);

            try (FileWriter file = new FileWriter(filePath)) 
            {
                file.write(userList.toJSONString());
                file.flush();
            } 
        }
        catch (Exception exception) 
        {
            exception.printStackTrace();
        }  
    }

    @SuppressWarnings("unchecked")
    private JSONArray jsonArray(String[] value)
    {
        JSONArray jsonArray = new JSONArray();

        for(int increment = 0; increment < value.length; increment++)
        {
            jsonArray.add(value[increment]);
        }

        return jsonArray;
    }

    @SuppressWarnings("unchecked")
   private String[] createAndReadLanguageFile(String value)
   {
        try 
        {
            File userFIle = new File("Language.json");

            JSONObject nlObject = new JSONObject();
            nlObject.put("1", "Voornaam");
            nlObject.put("2", "Achternaam");
            nlObject.put("3", "leeftijd");
            nlObject.put("4", "Gewicht");
            nlObject.put("5", "Lengte");
            nlObject.put("6", "Jaar");
            nlObject.put("7", "Profiel");
            nlObject.put("8", "Medicijn");
            nlObject.put("9", "Grafiek");
            nlObject.put("10", "Naam medicijn");
            nlObject.put("11", "Omschrijving");
            nlObject.put("12", "Soort");
            nlObject.put("13", "Dosering");
            nlObject.put("14", "Ik ben een patiënt");
            nlObject.put("15", "Ik ben een doctor");
            nlObject.put("16", "Bewerk");
            nlObject.put("17", "BMI");
            nlObject.put("18", "De patiënt daalde in gewicht.");
            nlObject.put("19", "De patiënt is in gewicht toegenomen.");
            nlObject.put("20", "Datum en tijd");
            nlObject.put("21", "Voeg nieuw gewicht toe.");

            JSONObject enObject = new JSONObject();
            enObject.put("1", "First name");
            enObject.put("2", "Last name");
            enObject.put("3", "Age");
            enObject.put("4", "Weight");
            enObject.put("5", "Length");
            enObject.put("6", "Years");
            enObject.put("7", "Profile");
            enObject.put("8", "Medicine");
            enObject.put("9", "Graph");
            enObject.put("10", "Name medicine");
            enObject.put("11", "Description");
            enObject.put("12", "Sort");
            enObject.put("13", "Dosage");
            enObject.put("14", "I am a patient.");
            enObject.put("15", "I am a doctor.");
            enObject.put("16", "Edit");
            enObject.put("17", "BMI");
            enObject.put("18", "Patient decreased in weight.");
            enObject.put("19", "Patient increased in weight.");
            enObject.put("20", "Date and Time");
            enObject.put("21", "Add new weight.");

            JSONObject frObject = new JSONObject();
            frObject.put("1", "Prénom");
            frObject.put("2", "nom de famille");
            frObject.put("3", "âge");
            frObject.put("4", "Poids");
            frObject.put("5", "Longueur");
            frObject.put("6", "ans");
            frObject.put("7", "Profil");
            frObject.put("8", "Médicament");
            frObject.put("9", "Graphique");
            frObject.put("10", "Nom du médicament");
            frObject.put("11", "Description");
            frObject.put("12", "Gentil");
            frObject.put("13", "Dosage");
            frObject.put("14", "Je suis un patient.");
            frObject.put("15", "Je suis médecin");
            frObject.put("16", "Éditer");
            frObject.put("17", "CMI");
            frObject.put("18", "Le patient a perdu du poids.");
            frObject.put("19", "Le patient a pris du poids.");
            frObject.put("20", "Date et l'heure");
            frObject.put("21", "Ajoutez un nouveau poids.");
            
            JSONObject nlJsonObject = new JSONObject();
            nlJsonObject.put("NL", nlObject);

            JSONObject enJsonObject = new JSONObject();
            enJsonObject.put("EN", enObject);

            JSONObject frJsonObject = new JSONObject();
            frJsonObject.put("FR", frObject);

            JSONArray jArray = new JSONArray();
            jArray.add(nlJsonObject);
            jArray.add(enJsonObject);
            jArray.add(frJsonObject);

            try (FileWriter file = new FileWriter(userFIle)) 
            {
                file.write(jArray.toJSONString());
                file.flush();
            } 

            int index;

            switch(value)
            {
                case "NL":
                    index = 0;
                    break;
                case "EN":
                    index = 1;
                    break;
                case "FR":
                    index = 2;
                    break;
                default:
                    index = 0;
            }

            JSONArray jsonArray = (JSONArray) jsonParser.parse(new FileReader(filePath1));
            JSONObject jsonObject = (JSONObject) jsonArray.get(index);
            JSONObject jsonObject2 = (JSONObject) jsonObject.get(value); 
            
            Set<?> set =  jsonObject2.keySet();

            Iterator<?> iterator = set.iterator();

            String[] languages = new String[set.size()];

            for(int increment = 0; increment < set.size(); increment++)
            {
                languages[increment] = (String) jsonObject2.get(iterator.next());
            }
            
            return languages; 
        } 
        catch (Exception exception) 
        {
            exception.printStackTrace();
        }  

        return new String[] {};
   }
   
    @SuppressWarnings("unchecked")
    private JSONArray createAndReadUserFile(String value)
    {
        try 
        {
            File userFIle = new File("PatientInfo.json");

            List<JSONArray> jsonArrays = new ArrayList<>();
            jsonArrays.add(jsonArray(new String[] {"John"}));
            jsonArrays.add(jsonArray(new String[] {"Doe"}));
            jsonArrays.add(jsonArray(new String[] {"45"}));
            jsonArrays.add(jsonArray(new String[] {"1.70"}));
            jsonArrays.add(jsonArray(new String[] {"A", "B", "C"}));

            JSONObject weightObject = new JSONObject();
            weightObject.put("2020-01-01 12:00:00","60.0");
            weightObject.put("2020-02-01 12:00:00","61.0");
            weightObject.put("2020-03-01 12:00:00","62.0");
            weightObject.put("2020-04-01 12:00:00","63.0");
            weightObject.put("2020-05-01 12:00:00","64.0");
            weightObject.put("2020-06-01 12:00:00","65.0");

            JSONObject userJsonObject = new JSONObject();
            userJsonObject.put("firstName", jsonArrays.get(0));
            userJsonObject.put("lastName", jsonArrays.get(1));
            userJsonObject.put("age", jsonArrays.get(2));
            userJsonObject.put("weight", weightObject);
            userJsonObject.put("length", jsonArrays.get(3));
            userJsonObject.put("medicineName", jsonArrays.get(4));
            userJsonObject.put("description", jsonArrays.get(4));
            userJsonObject.put("dosage", jsonArrays.get(4));
            userJsonObject.put("sort", jsonArrays.get(4));
            
            JSONObject userObject = new JSONObject();
            userObject.put(key, userJsonObject);

            JSONArray jArray = new JSONArray();
            jArray.add(userObject);

            try (FileWriter file = new FileWriter(userFIle)) 
            {
                file.write(jArray.toJSONString());
                file.flush();
            } 

            JSONArray jsonArray = (JSONArray) jsonParser.parse(new FileReader(filePath));

            JSONObject jsonObject = (JSONObject) jsonArray.get(0);  
            JSONObject jObject = (JSONObject) jsonObject.get(key);  
            
            return (JSONArray) jObject.get(value);
        } 
        catch (Exception exception) 
        {
            exception.printStackTrace();
        }

        return null;
    }
}