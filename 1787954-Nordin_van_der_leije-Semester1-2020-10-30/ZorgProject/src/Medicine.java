final class Medicine
{
    private String medicineName;
    private String description;
    private String sort;
    private String dosage;

    protected String getMedicineName()
    {
        return medicineName;
    }
        
    protected void setMedicineName(String name)
    {
        this.medicineName = name;
    }

    protected String getDescription()
    {
        return description;
    }

    protected void setDescription(String name)
    {
        this.description = name;
    }

    protected String getSort()
    {
        return sort;
    }

    protected void setSort(String name)
    {
        this.sort = name;
    }

    protected String getDosage()
    {
        return dosage;
    }

    protected void setDosage(String name)
    {
        this.dosage = name;
    }
}